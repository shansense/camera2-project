package com.linzs.mycamera2kotlin

import android.content.Context
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.util.Log

class Camera2Helper(context: Context) {
    private val TAG = "Camera2Helper-Linzs"
    private val context:Context
    private val cameraManager:CameraManager

    init {
        this.context = context
        cameraManager = this.context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
    }

    /**
     * 获取所有摄像头的CameraID
     */
    fun getCameraIds(): Array<String> {
        return cameraManager.cameraIdList
    }

    /**
     * 获取摄像头方向
     */
    fun getCameraOrientationString(cameraId: String): String {
        val characteristics = cameraManager.getCameraCharacteristics(cameraId)
        val lensFacing = characteristics.get(CameraCharacteristics.LENS_FACING)!!
        return when (lensFacing) {
            CameraCharacteristics.LENS_FACING_BACK -> "后摄(Back)"
            CameraCharacteristics.LENS_FACING_FRONT -> "前摄(Front)"
            CameraCharacteristics.LENS_FACING_EXTERNAL -> "外置(External)"
            else -> "Unknown"
        }
    }




}