package com.linzs.mycamera2kotlin

import android.content.Context
import android.hardware.camera2.CameraManager
import android.os.Bundle
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity-Linzs"
    private lateinit var camera2Helper: Camera2Helper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //申请权限
        ActivityCompat.requestPermissions(
            this@MainActivity,
            arrayOf(
                android.Manifest.permission.CAMERA,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.RECORD_AUDIO
            ), 123
        )

        camera2Helper = Camera2Helper(this)


        val cameraIds = camera2Helper.getCameraIds()

        cameraIds.forEach { cameraId ->
            val orientation = camera2Helper.getCameraOrientationString(cameraId)
            Log.i(TAG, "cameraId : $cameraId - $orientation")
        }


    }


}