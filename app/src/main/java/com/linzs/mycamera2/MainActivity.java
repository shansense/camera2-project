package com.linzs.mycamera2;


import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.linzs.mycamera2.camer2helper.Camera2Helper;
import com.linzs.mycamera2.camer2helper.Camera2Helper2;
import com.linzs.mycamera2.view.AutoFitTextureView;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements Camera2Helper.ImageCallback,Camera2Helper2.ImageCallback {
    private AutoFitTextureView mTextureView,mTextureView2;
    private ImageView iv_show;
    private Camera2Helper mCamera2Helper;
    private Camera2Helper2 mCamera2Helper2;
    private Button StartBtn, StopBtn;

    private int flage = 3;
    private int flage2 = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Hello", "{MainActivity}onCreate == >");
        setContentView(R.layout.activity_main);
        initVIew();
        //声明权限
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Hello", "{MainActivity}onResume == >");
    }

    private void initVIew() {

        StartBtn = findViewById(R.id.start);
        StopBtn = findViewById(R.id.stop);

        mTextureView2 = findViewById(R.id.textureView2);
        mTextureView2.setAspectRation(mTextureView2.getWidth(),mTextureView2.getHeight());
        mCamera2Helper2 = new Camera2Helper2(this, mTextureView2);
        //设置回调
        mCamera2Helper2.setCallBack(this);


//        iv_show = (ImageView) findViewById(R.id.iv_show_camera2_activity);
        //mSurfaceView
        mTextureView = findViewById(R.id.textureView);
        mTextureView.setAspectRation(mTextureView.getWidth(),mTextureView.getHeight());

        mCamera2Helper = new Camera2Helper(this, mTextureView);
        //设置回调
        mCamera2Helper.setCallBack(this);


        StartBtn.setOnClickListener(v -> {

            if (flage == 1) {
                //开启
                mCamera2Helper.initCamera2();
                flage = 0;
            }
            if (flage2 == 1) {
                mCamera2Helper2.initCamera2();
                flage2 = 0;
            }
        });

        StopBtn.setOnClickListener(v -> {
                mCamera2Helper.cancelCamera();
                mCamera2Helper2.cancelCamera();

        });
    }





    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Hello", "{MainActivity}onPause == >");

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Hello", "{MainActivity}onStop == >");
        mCamera2Helper.cancelCamera();
        mCamera2Helper2.cancelCamera();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Hello", "{MainActivity}onDestroy == >");
        if (mCamera2Helper != null) {
            mCamera2Helper.release();
        }
        if (mCamera2Helper2 != null) {
            mCamera2Helper2.release();
        }
        mTextureView = null;
        mTextureView2 = null;
    }

//    @Override
//    public void getBitImage(Bitmap bitmap) {
//        mTextureView.setVisibility(View.GONE);
//        iv_show.setImageBitmap(bitmap);
//    }

    @Override
    public void getStatus(int m) {
        if (m == 1) {
            flage = 1;
        }
    }

    @Override
    public void getStatus2(int m) {
        if (m == 1) {
            flage2 = 1;
        }
    }
}