package com.linzs.mycamera2.camer2helper;


import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.linzs.mycamera2.view.AutoFitTextureView;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class Camera2Helper2 {
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    //为了使照片竖直显示
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private Surface mSurface;
    private Context mContext;
    private AutoFitTextureView mAutoFitTextureView;
    private Handler childHandler, mainHandler;
    private CameraManager mCameraManager;//摄像头管理器
    private String mCameraID;//摄像头Id 0 为后  1 为前
    private ImageReader mImageReader;
    public CameraDevice mCameraDevice;
    private CameraCaptureSession mCameraCaptureSession;
    private HandlerThread mHandlerThread;



    public Camera2Helper2(Context context, AutoFitTextureView autoFitTextureView) {
        mContext = context;
        mAutoFitTextureView = autoFitTextureView;
        mAutoFitTextureView.setSurfaceTextureListener(listener);

    }

    TextureView.SurfaceTextureListener listener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            // 在这里开始渲染到SurfaceTexture上
            mImageCallback.getStatus2(1);
            mSurface = new Surface(mAutoFitTextureView.getSurfaceTexture());
//            initCamera2();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            // 在这里处理SurfaceTexture大小的变化
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            // 在这里停止渲染到SurfaceTexture上并释放资源
            Log.d("Hello", "{Camera2Helper}onSurfaceTextureDestroyed == >");
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }

            return true; // 返回true表示已经处理了销毁事件
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
            // 这不是用来开始或停止渲染的，但可以用来处理帧的更新
        }
    };



    //监听摄像头是否启动
    private CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice camera) { //打开摄像
            Log.d("Hello", "{MainActivity}onOpened == > 摄像头已打开");
            mCameraDevice = camera;
            //开启预览
            takePreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {  //断开摄像头
            Log.d("Hello", "{Camera2Helper}onDisconnected == > 摄像头已断开");
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) { //发生错误
            Toast.makeText(mContext, "摄像头开启失败", Toast.LENGTH_SHORT).show();
        }

    };


    /**
     * 数据初始化
     */
    public void initCamera2() {
        mHandlerThread = new HandlerThread("Camera2");
        mHandlerThread.start();
        childHandler = new Handler(mHandlerThread.getLooper());  //子线程消息处理
        mainHandler = new Handler(Looper.getMainLooper()); //主线程消息处理
        mCameraID = "" + CameraCharacteristics.LENS_FACING_FRONT;//后摄像头
        mImageReader = ImageReader.newInstance(1080, 1920, ImageFormat.JPEG,1);//mImageReader 用于获取照片

        //获得预览回调
        mImageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() { //可以在这里处理拍照得到的临时照片 例如，写入本地
            @Override
            public void onImageAvailable(ImageReader reader) {
                Log.d("Hello", "{MainActivity}onImageAvailable == >");
                mCameraDevice.close();
                // 拿到拍照照片数据
                Image image = reader.acquireNextImage();
                ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                byte[] bytes = new byte[buffer.remaining()];
                buffer.get(bytes);//由缓冲区存入字节数组
                final Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                if (bitmap != null) {
//                    mImageCallback.getBitImage(bitmap);
                }
            }
        }, mainHandler);
        //获取摄像头管理
        mCameraManager = (CameraManager) mContext.getSystemService(Context.CAMERA_SERVICE);
        try {
            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            //打开摄像头
            mCameraManager.openCamera("1", stateCallback, childHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    /**
     * 开始预览
     */
    private void takePreview() {
        try {
            // 创建预览需要的CaptureRequest.Builder
            final CaptureRequest.Builder previewRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            // 将SurfaceView的surface作为CaptureRequest.Builder的目标
            previewRequestBuilder.addTarget(mSurface);
            // 创建CameraCaptureSession，该对象负责管理处理预览请求和拍照请求
            mCameraDevice.createCaptureSession(Arrays.asList(mSurface, mImageReader.getSurface()), new CameraCaptureSession.StateCallback() // ③
            {
                @Override
                public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                    if (null == mCameraDevice) return;
                    // 当摄像头已经准备好时，开始显示预览
                    mCameraCaptureSession = cameraCaptureSession;
                    try {
                        // 自动对焦
                        previewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                        // 打开闪光灯
//                        previewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
                        // 显示预览
                        CaptureRequest previewRequest = previewRequestBuilder.build();
                        mCameraCaptureSession.setRepeatingRequest(previewRequest, null, childHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
                    Log.d("Hello", "{Camera2Helper}onConfigureFailed == > 配置失败");
//                    Toast.makeText(MainActivity.this, "配置失败", Toast.LENGTH_SHORT).show();
                }
            }, childHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }



    /**
     * 拍照
     */
    public void takePicture() {
        Log.d("Hello", "{Camera2Helper}takePicture == >" + mCameraDevice);
        if (mCameraDevice == null) return;
        // 创建拍照需要的CaptureRequest.Builder
        final CaptureRequest.Builder captureRequestBuilder;
        try {
            captureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            // 将imageReader的surface作为CaptureRequest.Builder的目标,也就是预览输出
            captureRequestBuilder.addTarget(mImageReader.getSurface());
            // 自动对焦
            captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            // 自动曝光
            captureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
            // 设置自动白平衡
            captureRequestBuilder.set(CaptureRequest.CONTROL_AWB_MODE, CaptureRequest.CONTROL_AWB_MODE_AUTO);
            // 获取手机方向
            int rotation = ((Activity)mContext).getWindowManager().getDefaultDisplay().getRotation();
            Log.d("Hello", "{Camera2Helper}takePicture == >" + rotation);
            // 根据设备方向计算设置照片的方向
            captureRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));
            //拍照
            CaptureRequest mCaptureRequest = captureRequestBuilder.build();
            mCameraCaptureSession.capture(mCaptureRequest, null, childHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    public synchronized void stop() {
        if (mCameraDevice == null) {
            return;
        }
        Log.d("Hello", "{Camera2Helper}stop == > 213131");

//        cancelCamera();
        stopBackgroundThread();
    }

    public synchronized void cancelCamera() {
        Log.v("TAG", "cancelCamera");
//        if (null != mCameraCaptureSession) {
//            mCameraCaptureSession.close();
//            mCameraCaptureSession = null;
//        }

        if (mCameraDevice != null) {
            mCameraDevice.close();
            mCameraDevice = null;
        }

//        if (mImageReader != null) {
//            mImageReader.close();
//            mImageReader = null;
//        }

        mImageCallback.getStatus2(1);
    }

    private synchronized void stopBackgroundThread() {
        Log.v("TAG", "stopBackgroundThread");
        mHandlerThread.quitSafely();
        try {
            mHandlerThread.join();
            mHandlerThread = null;
            childHandler = null;
            mainHandler = null;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized void release() {
        stop();
        mAutoFitTextureView = null;
        mContext = null;
        mImageCallback = null;
        mSurface.release();
        mSurface = null;

    }

    private ImageCallback mImageCallback;
    public interface ImageCallback {
//        void getBitImage(Bitmap bitmap);
        void getStatus2(int m);
    }
    public void setCallBack(ImageCallback mCallBack) {
        mImageCallback = mCallBack;
    }

}
